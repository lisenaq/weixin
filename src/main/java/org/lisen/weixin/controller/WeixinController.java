package org.lisen.weixin.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.weixin4j.spring.web.WeixinJieruController;

/**
 * @author Administrator
 * @create 2020-03-0621:57
 */
@Controller
@RequestMapping("/weixin/accesspoint")
public class WeixinController extends WeixinJieruController {
}
