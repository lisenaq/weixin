<!--
assign:  使用该指令你可以创建一个新的变量， 或者替换一个已经存在的变量。
-->
<#assign ctx=request.contextPath />

<!DOCTYPE html>
<html lang="en">
<head>
    <title>weixin</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
</head>
<body>
<h1>Hello weixin,</h1><br data-tomark-pass>
<p>当前时间：${.now?string("yyyy-MM-dd HH:mm:ss.sss")}</p>
<p>
    <a href="${ctx}/commonGrammar">常用语法</a>
</p>
</body>
</html>